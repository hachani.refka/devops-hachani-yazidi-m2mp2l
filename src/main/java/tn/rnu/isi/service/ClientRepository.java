package tn.rnu.isi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tn.rnu.isi.model.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

	
	
	Client findByIdClient(Long idClient);
	
	 
	List<Client> findAll();
	
	Client save (Client client);
	 
	@Modifying
	@Query("update Client cl set cl.idClient = ?1")
	int updateIdClient(Long idClient);
	
	@Modifying
	@Query("update Client cl set cl.loginClient = ?1, cl.motPasseClient = ?2 ,cl.nomClient= ?3 , cl.prenomClient= ?4 , cl.civiliteClient= ?5 , cl.dateNaissanceClient= ?6 , cl.numeroAdrClient= ?7 , cl.rueAdrClient = ?8 , cl.communeAdrClient = ?9 , cl.villeAdrClient = ?10 , "
			+ "cl.cpAdrClient = ?11 , cl.telClient = ?12 , cl.faxClient = ?13 , cl.gsmClient = ?14 , cl.emailClient = ?15 "
			+ " where cl.idClient = ?15")
	int updateDesigClient ( String loginClient, String motPasseClient, String nomClient, String prenomClient,
			String civiliteClient, String dateNaissanceClient, String numeroAdrClient, String rueAdrClient,
			String communeAdrClient, String villeAdrClient, String cpAdrClient, String telClient, String faxClient,
			String gsmClient, String emailClient);

 	@Transactional
 	@Modifying
	@Query("delete from Client cl where cl.idClient = ?1")
	void deleteClientByIdProduit(Long idClient);
  
}
