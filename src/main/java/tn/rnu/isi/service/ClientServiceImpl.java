package tn.rnu.isi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.rnu.isi.model.Client;
import tn.rnu.isi.model.Produit;

@Service
@Transactional
public class ClientServiceImpl  implements ClientService{
	
	@Autowired
	private ClientRepository clientRepository ;  

	@Override
	public Long save(Client client) throws Exception {
		
		  clientRepository.save(client);
		  return client.getIdClient() ; 
	}

	@Override
	public List<Client> getAll() {
		return (List<Client>) clientRepository.findAll() ;
	}

	@Override
	public Client getByIdClient(Long idClient) throws Exception {
		return (Client) clientRepository.findByIdClient(idClient);
	}

	@Override
	public int updateId(Long idClient) {
		
		return clientRepository.updateIdClient(idClient); 
	}

	@Override
	public int updateDesigClient(String loginClient, String motPasseClient, String nomClient, String prenomClient,
			String civiliteClient, String dateNaissanceClient, String numeroAdrClient, String rueAdrClient,
			String communeAdrClient, String villeAdrClient, String cpAdrClient, String telClient, String faxClient,
			String gsmClient, String emailClient) {
		// TODO Auto-generated method stub
		return clientRepository.updateDesigClient(loginClient, motPasseClient, nomClient, prenomClient, civiliteClient, dateNaissanceClient, numeroAdrClient, rueAdrClient, communeAdrClient,
				villeAdrClient, cpAdrClient, telClient, faxClient, gsmClient, emailClient); 
	}

	//@Override
//	public void deleteCommandeByIdClient(Long idClient) {
//			clientRepository.deleteById(idClient);
//	}

	@Override
	public void deleteClient(Long idClient) {

	clientRepository.deleteById(idClient);

	}

}
